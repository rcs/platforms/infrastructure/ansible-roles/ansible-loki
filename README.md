Ansible Loki
============

Grafana Loki is a set of components that can be composed into a fully featured logging stack.

Unlike other logging systems, Loki is built around the idea of only indexing metadata about your logs: labels (just like Prometheus labels). Log data itself is then compressed and stored in chunks in object stores such as S3 or GCS, or even locally on the filesystem. A small index and highly compressed chunks simplifies the operation and significantly lowers the cost of Loki.

Requirements
------------

Any pre-requisites that may not be covered by Ansible itself or the role should
be mentioned here. For instance, if the role uses the EC2 module, it may be a
good idea to mention in this section that the boto package is required.

Role Variables
--------------

### Default Variables
```
loki_user: loki
loki_group: loki

loki_base_dir: /opt/loki
loki_dir: "{{ loki_base_dir }}"
loki_config_dir: /etc/loki
loki_version: "1.3.0"

loki_dist_url: "https://github.com/grafana/loki/releases/download/v{{ loki_version }}/loki-linux-amd64.zip"
loki_dist_location: "{{ loki_base_dir }}/loki-linux-amd64.zip"

## config file variables

loki_auth_enabled: "false"

loki_http_listen_port: 3100
```

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in
regards to parameters that may need to be set for other roles, or variables that
are used from other roles.

Example Playbook
----------------

    - hosts: servers
      roles:
     - {
         role: "ansible-loki",
         loki_version: "1.2.0"
         loki_user:   loki,
         loki_group:  loki,
         loki_base_dir: /opt/loki
       }

License
-------

BSD

Author Information
------------------

RCS @ university of Cambridge.
